define(['custom'], function(custom) {

	
	var fileName  = 'slider';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $firebase) {	   
				    
   
			
			            // 1st ngRepeat demo
			            $scope.slides = [	{id: 1, img: 'media/home/mockSlide1.jpg', label: 'Custom Websites That Works For Any Platform', subtitle: "Whether it's for mobile, tablet or desktops."},
			            					{id: 2, img: 'media/home/mockSlide2.jpg', label: 'Built to last.', subtitle: "Do what you want to do.  Semantic and easy markup makes editing your site a piece of cake."},
			            					{id: 3, img: 'media/home/mockSlide3.jpg', label: 'No project too big or too small.', subtitle: "Need a personal site?  No problem.  Are you a corporation that needs something more complex?  No problem!  We work with you to deliever what you need."},
			            				];				    
				    

				

				   
				    
				});				
	    },
	    ///////////////////////////////////////
  };
});
