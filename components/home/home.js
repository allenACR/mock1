define(['custom'], function(custom) {

	
	var fileName  = 'home';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $timeout, cfpLoadingBar, $firebase, psResponsive) {	   
				    
					// INIT
					$scope.init = function(){
						 cfpLoadingBar.start();
					};	
					
					$timeout(function(){
						 cfpLoadingBar.complete();
					}, 1000);
				
					$scope.lorem = "Lorem ipsum dolor sit amet, an epicuri mediocrem vituperata eos. Illud volumus periculis per no, cu torquatos definitionem eum. Est cu hendrerit vituperatoribus. In officiis nominati eum, aperiri dolorem usu ut. In meliore denique suavitate vim.";
					
				
	    
				});				
	    },
	    ///////////////////////////////////////
  };
});
